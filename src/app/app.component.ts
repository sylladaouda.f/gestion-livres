import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(){
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyCb5XgoqXj4wazsJdu_PxI6EZNstkGKfh0",
      authDomain: "gestion-livres-40709.firebaseapp.com",
      databaseURL: "https://gestion-livres-40709.firebaseio.com",
      projectId: "gestion-livres-40709",
      storageBucket: "gestion-livres-40709.appspot.com",
      messagingSenderId: "666770005447"
    };
    firebase.initializeApp(config);
  }
}
