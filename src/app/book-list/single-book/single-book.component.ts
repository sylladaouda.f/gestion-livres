import { BooksService } from './../../services/books.service';
import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-single-book',
  templateUrl: './single-book.component.html',
  styleUrls: ['./single-book.component.sass']
})
export class SingleBookComponent implements OnInit {

  book : Book;

  /**
   * 
   * @param booksService : Injection du service de gestion des books
   * @param route : Injection de la route active
   * @param router : Injection du router
   */
  constructor(private booksService : BooksService,
              private route : ActivatedRoute,
              private router : Router) { }

  ngOnInit() {
    this.book = new Book('', '');
    //Récupération du paramètre de la route
    const id = this.route.snapshot.params['id'];
    this.booksService.getSingleBook(+id).then(
      (book : Book) => {
        this.book = book;
      }
    );
  }

  /**
   * Retour à la liste des books
   */
  onBack(){
    this.router.navigate(['/books']);
  }

}
