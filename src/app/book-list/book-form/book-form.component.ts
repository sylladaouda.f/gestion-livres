import { Book } from './../../models/book.model';
import { Router } from '@angular/router';
import { BooksService } from './../../services/books.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.sass']
})
export class BookFormComponent implements OnInit {

  bookForm : FormGroup;
  fileIsUploading = false;
  fileUrl : string;
  fileUploaded = false

  constructor(private booksService : BooksService,
              private formBuilder : FormBuilder,
              private router : Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.bookForm = this.formBuilder.group(
      {
        title : ['', Validators.required],
        author : ['', Validators.required]
      }
    );
  }

  /**
   * Ajout d'un nouveau book
   */
  onSaveBook(){
    const title = this.bookForm.get('title').value;
    const author = this.bookForm.get('author').value;
    const newBook = new Book(title, author)
    if (this.fileUrl && this.fileUrl !== '') {
      newBook.photo = this.fileUrl;
    }
    this.booksService.createNewBook(newBook);
    this.router.navigate(['/books']);
  }

  /**
   * Chargement du fichier
   * @param file 
   */
  onUploadFile(file : File){
    this.fileIsUploading = true;
    this.booksService.uploadFile(file).then(
      (url : string) => {
        this.fileUrl = url;
        this.fileIsUploading = false;
        this.fileUploaded = true;
      }
    );
  }

  /**
   * Gestion des inputs de type File
   * @param event 
   */
  detectFiles(event){
    this.onUploadFile(event.target.files[0]);
  }

}
