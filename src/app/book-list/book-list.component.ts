import { Router } from '@angular/router';
import { BooksService } from './../services/books.service';
import { Book } from './../models/book.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.sass']
})
export class BookListComponent implements OnInit, OnDestroy {

  books : Book[];
  //L'object de souscription à notre observable suject
  booksSubscription : Subscription;

  constructor(private bookService : BooksService, private router : Router) { }

  ngOnInit() {
    this.booksSubscription = this.bookService.booksSubject.subscribe(
      (books : Book[]) => {
        this.books = books;
      }
    );
    this.bookService.getBooks();
    this.bookService.emitBooks();
  }

  /**
   * Accès à l'ajout d'un book
   */
  onNewBook(){
    this.router.navigate(['/books', 'new']);
  }

  /**
   * Supprimer un book
   * @param book 
   */
  onDeleteBook(book : Book){
    this.bookService.removeBook(book);
  }

  /**
   * Détail d'un book
   * @param id 
   */
  onViewBook(id : number){
    this.router.navigate(['/books', 'view', id]);
  }

  ngOnDestroy(): void {
    this.booksSubscription.unsubscribe();
  }

}
