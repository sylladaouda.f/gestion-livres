import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  // Indique l'état d'authentifiaction
  isAuth : boolean;

  constructor(private authService : AuthService,
    private router : Router) { }

  ngOnInit() {
    //Indique si l'utilisateur est connecté
    firebase.auth().onAuthStateChanged(
      (user) => {
        if (user) {
          this.isAuth = true;
        }else {
          this.isAuth = false;
        }
      }
    );
  }

  /**
   * Déconnecter l'utilisateur
   */
  onSignOut(){
    this.authService.signOutUser();
    this.router.navigate(['/auth', 'signin']);
  }

}
