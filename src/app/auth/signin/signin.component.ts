import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.sass']
})
export class SigninComponent implements OnInit {

  //Attribut représentant notre formulaire (méthode réative pour la création de formulaires) 
  signInForm : FormGroup;

  //Message d'erreur si la connexion échoue
  errorMessage : string;

  /**
   * 
   * @param formBuilder : Injection de l'objet permettant de créer les attributs d'un formulaire
   * @param authService : Injection du service d'auth
   * @param router : Injection du service de routage
   */
  constructor(private formBuilder : FormBuilder, 
    private authService : AuthService,
    private router : Router) { }

  ngOnInit() {
    //Init des attributs du formulaire à la création du component
    this.initForm();
  }

  /**
   * Création des attributs du formulaire
   */
  initForm() {
    this.signInForm = this.formBuilder.group(
      {
        email : ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
      }
    );
  }

  /**
   * Evénement de demande de connexion d'un utilisateur
   */
  onSubmit(){
    const email = this.signInForm.get('email').value;
    const password = this.signInForm.get('password').value;
    this.authService.signInUser(email, password).then(
      () => {
        this.router.navigate(['/books']);
      },
      (error) => {
        this.errorMessage = error;
      }
    )
  }

}
