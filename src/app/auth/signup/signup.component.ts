import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.sass']
})
export class SignupComponent implements OnInit {

  //Attribut représentant notre formulaire (méthode réative pour la création de formulaires) 
  signUpForm : FormGroup;

  //Message d'erreur si la création échoue
  errorMessage : string;

  /**
   * 
   * @param formBuilder : Injection de l'objet permettant de créer les attributs d'un formulaire
   * @param authService : Injection du service d'auth
   * @param router : Injection du service de routage
   */
  constructor(private formBuilder : FormBuilder, 
    private authService : AuthService,
    private router : Router) { }

  ngOnInit() {
    //Init des attributs du formulaire à la création du component
    this.initForm();
  }

  /**
   * Création des attributs du formulaire
   */
  initForm() {
    this.signUpForm = this.formBuilder.group(
      {
        email : ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{6,}/)]]
      }
    );
  }

  /**
   * Evénement de demande d'enregistrement d'un nouvel utilisateur
   */
  onSubmit(){
    const email = this.signUpForm.get('email').value;
    const password = this.signUpForm.get('password').value;
    this.authService.createNewUser(email, password).then(
      () => {
        this.router.navigate(['/books']);
      },
      (error) => {
        this.errorMessage = error;
      }
    )
  }

}
