import { Injectable } from '@angular/core';
import { Book } from '../models/book.model';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';
import { resolve, reject } from 'q';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  books : Book[] = [];

  //Observable permettant d'émettre une liste de book aux subscribe
  booksSubject = new Subject<Book[]>();

  constructor() { }

  /**
   * Méthode permettant d'émettre les books reçus par le subject
   */
  emitBooks(){
    this.booksSubject.next(this.books);
  }

  /**
   * Ajout des books dans la base de données
   */
  saveBook(){
    firebase.database().ref('/books').set(this.books);
  }

  /**
   * Récupération des books
   */
  getBooks(){
    firebase.database().ref('/books')
    .on('value', (data) => {
      this.books = data.val() ? data.val() : [];
      this.emitBooks();
    })
  }

  /**
   * Récup^ération d'un book via son id
   * @param id 
   */
  getSingleBook(id : number) {
    return new Promise(
      (resolve, reject) => {
        firebase.database().ref('/books/' + id)
        .once('value').then(
          (data) => {
            resolve(data.val());
          },
          (error) => {
            reject(error);
          }
        );
      }
    );
  }


  /**
   * Ajout d'un book dans la liste des book
   * @param newBook 
   */
  createNewBook(newBook : Book){
    this.books.push(newBook);
    this.saveBook();
    this.emitBooks();
  }

  /**
   * Supprimer un book
   * @param book 
   */
  removeBook(book : Book){
    if(book.photo){
      const storageRef = firebase.storage().refFromURL(book.photo);
      storageRef.delete().then(
        () => {
          console.log("Photo supprimée !");
          
        }
      )
      .catch(
        (error) => {
          console.log("Photo nom trouvée", error);
          
        }
      );
    }
    const bookIndexToremove = this.books.findIndex(
      (bookEl) => {
        if (bookEl === book) {
          return true;
        }
      }
    );
    this.books.splice(bookIndexToremove, 1);
    this.saveBook();
    this.emitBooks();
  }

  uploadFile(file : File){
    return new Promise(
      (resolve, reject) => {
        const almostUniqueFileName = Date.now().toString();
        const upload = firebase.storage().ref()
        .child('images/' + almostUniqueFileName + file.name)
        .put(file);
        upload.on(firebase.storage.TaskEvent.STATE_CHANGED,
          () => {
            console.log("Chargement ...");
            
          },
          (error) => {
            console.log("Erreur de chargement : ", error);
            reject();
          },
          () => {
            upload.snapshot.ref.getDownloadURL().then(
              (url : string) => {
                resolve(url);
              }
            );
          }
        );
      }
    );
  }

}
